// Code generated by protoc-gen-go. DO NOT EDIT.
// source: wiki.proto

package gitaly

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type WikiCommitDetails struct {
	Name    []byte `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	Email   []byte `protobuf:"bytes,2,opt,name=email,proto3" json:"email,omitempty"`
	Message []byte `protobuf:"bytes,3,opt,name=message,proto3" json:"message,omitempty"`
}

func (m *WikiCommitDetails) Reset()                    { *m = WikiCommitDetails{} }
func (m *WikiCommitDetails) String() string            { return proto.CompactTextString(m) }
func (*WikiCommitDetails) ProtoMessage()               {}
func (*WikiCommitDetails) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{0} }

func (m *WikiCommitDetails) GetName() []byte {
	if m != nil {
		return m.Name
	}
	return nil
}

func (m *WikiCommitDetails) GetEmail() []byte {
	if m != nil {
		return m.Email
	}
	return nil
}

func (m *WikiCommitDetails) GetMessage() []byte {
	if m != nil {
		return m.Message
	}
	return nil
}

type WikiPageVersion struct {
	Commit *GitCommit `protobuf:"bytes,1,opt,name=commit" json:"commit,omitempty"`
	Format string     `protobuf:"bytes,2,opt,name=format" json:"format,omitempty"`
}

func (m *WikiPageVersion) Reset()                    { *m = WikiPageVersion{} }
func (m *WikiPageVersion) String() string            { return proto.CompactTextString(m) }
func (*WikiPageVersion) ProtoMessage()               {}
func (*WikiPageVersion) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{1} }

func (m *WikiPageVersion) GetCommit() *GitCommit {
	if m != nil {
		return m.Commit
	}
	return nil
}

func (m *WikiPageVersion) GetFormat() string {
	if m != nil {
		return m.Format
	}
	return ""
}

type WikiPage struct {
	// These fields are only present in the first message of a WikiPage stream
	Version    *WikiPageVersion `protobuf:"bytes,1,opt,name=version" json:"version,omitempty"`
	Format     string           `protobuf:"bytes,2,opt,name=format" json:"format,omitempty"`
	Title      []byte           `protobuf:"bytes,3,opt,name=title,proto3" json:"title,omitempty"`
	UrlPath    string           `protobuf:"bytes,4,opt,name=url_path,json=urlPath" json:"url_path,omitempty"`
	Path       []byte           `protobuf:"bytes,5,opt,name=path,proto3" json:"path,omitempty"`
	Name       []byte           `protobuf:"bytes,6,opt,name=name,proto3" json:"name,omitempty"`
	Historical bool             `protobuf:"varint,7,opt,name=historical" json:"historical,omitempty"`
	// This field is present in all messages of a WikiPage stream
	RawData []byte `protobuf:"bytes,8,opt,name=raw_data,json=rawData,proto3" json:"raw_data,omitempty"`
}

func (m *WikiPage) Reset()                    { *m = WikiPage{} }
func (m *WikiPage) String() string            { return proto.CompactTextString(m) }
func (*WikiPage) ProtoMessage()               {}
func (*WikiPage) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{2} }

func (m *WikiPage) GetVersion() *WikiPageVersion {
	if m != nil {
		return m.Version
	}
	return nil
}

func (m *WikiPage) GetFormat() string {
	if m != nil {
		return m.Format
	}
	return ""
}

func (m *WikiPage) GetTitle() []byte {
	if m != nil {
		return m.Title
	}
	return nil
}

func (m *WikiPage) GetUrlPath() string {
	if m != nil {
		return m.UrlPath
	}
	return ""
}

func (m *WikiPage) GetPath() []byte {
	if m != nil {
		return m.Path
	}
	return nil
}

func (m *WikiPage) GetName() []byte {
	if m != nil {
		return m.Name
	}
	return nil
}

func (m *WikiPage) GetHistorical() bool {
	if m != nil {
		return m.Historical
	}
	return false
}

func (m *WikiPage) GetRawData() []byte {
	if m != nil {
		return m.RawData
	}
	return nil
}

type WikiGetPageVersionsRequest struct {
	Repository *Repository `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
	PagePath   []byte      `protobuf:"bytes,2,opt,name=page_path,json=pagePath,proto3" json:"page_path,omitempty"`
}

func (m *WikiGetPageVersionsRequest) Reset()                    { *m = WikiGetPageVersionsRequest{} }
func (m *WikiGetPageVersionsRequest) String() string            { return proto.CompactTextString(m) }
func (*WikiGetPageVersionsRequest) ProtoMessage()               {}
func (*WikiGetPageVersionsRequest) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{3} }

func (m *WikiGetPageVersionsRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *WikiGetPageVersionsRequest) GetPagePath() []byte {
	if m != nil {
		return m.PagePath
	}
	return nil
}

type WikiGetPageVersionsResponse struct {
	Versions []*WikiPageVersion `protobuf:"bytes,1,rep,name=versions" json:"versions,omitempty"`
}

func (m *WikiGetPageVersionsResponse) Reset()                    { *m = WikiGetPageVersionsResponse{} }
func (m *WikiGetPageVersionsResponse) String() string            { return proto.CompactTextString(m) }
func (*WikiGetPageVersionsResponse) ProtoMessage()               {}
func (*WikiGetPageVersionsResponse) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{4} }

func (m *WikiGetPageVersionsResponse) GetVersions() []*WikiPageVersion {
	if m != nil {
		return m.Versions
	}
	return nil
}

// This message is sent in a stream because the 'content' field may be large.
type WikiWritePageRequest struct {
	// These following fields are only present in the first message.
	Repository    *Repository        `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
	Name          []byte             `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	Format        string             `protobuf:"bytes,3,opt,name=format" json:"format,omitempty"`
	CommitDetails *WikiCommitDetails `protobuf:"bytes,4,opt,name=commit_details,json=commitDetails" json:"commit_details,omitempty"`
	// This field is present in all messages.
	Content []byte `protobuf:"bytes,5,opt,name=content,proto3" json:"content,omitempty"`
}

func (m *WikiWritePageRequest) Reset()                    { *m = WikiWritePageRequest{} }
func (m *WikiWritePageRequest) String() string            { return proto.CompactTextString(m) }
func (*WikiWritePageRequest) ProtoMessage()               {}
func (*WikiWritePageRequest) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{5} }

func (m *WikiWritePageRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *WikiWritePageRequest) GetName() []byte {
	if m != nil {
		return m.Name
	}
	return nil
}

func (m *WikiWritePageRequest) GetFormat() string {
	if m != nil {
		return m.Format
	}
	return ""
}

func (m *WikiWritePageRequest) GetCommitDetails() *WikiCommitDetails {
	if m != nil {
		return m.CommitDetails
	}
	return nil
}

func (m *WikiWritePageRequest) GetContent() []byte {
	if m != nil {
		return m.Content
	}
	return nil
}

type WikiWritePageResponse struct {
	DuplicateError []byte `protobuf:"bytes,1,opt,name=duplicate_error,json=duplicateError,proto3" json:"duplicate_error,omitempty"`
}

func (m *WikiWritePageResponse) Reset()                    { *m = WikiWritePageResponse{} }
func (m *WikiWritePageResponse) String() string            { return proto.CompactTextString(m) }
func (*WikiWritePageResponse) ProtoMessage()               {}
func (*WikiWritePageResponse) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{6} }

func (m *WikiWritePageResponse) GetDuplicateError() []byte {
	if m != nil {
		return m.DuplicateError
	}
	return nil
}

type WikiUpdatePageRequest struct {
	// There fields are only present in the first message of the stream
	Repository    *Repository        `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
	PagePath      []byte             `protobuf:"bytes,2,opt,name=page_path,json=pagePath,proto3" json:"page_path,omitempty"`
	Title         []byte             `protobuf:"bytes,3,opt,name=title,proto3" json:"title,omitempty"`
	Format        string             `protobuf:"bytes,4,opt,name=format" json:"format,omitempty"`
	CommitDetails *WikiCommitDetails `protobuf:"bytes,5,opt,name=commit_details,json=commitDetails" json:"commit_details,omitempty"`
	// This field is present in all messages
	Content []byte `protobuf:"bytes,6,opt,name=content,proto3" json:"content,omitempty"`
}

func (m *WikiUpdatePageRequest) Reset()                    { *m = WikiUpdatePageRequest{} }
func (m *WikiUpdatePageRequest) String() string            { return proto.CompactTextString(m) }
func (*WikiUpdatePageRequest) ProtoMessage()               {}
func (*WikiUpdatePageRequest) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{7} }

func (m *WikiUpdatePageRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *WikiUpdatePageRequest) GetPagePath() []byte {
	if m != nil {
		return m.PagePath
	}
	return nil
}

func (m *WikiUpdatePageRequest) GetTitle() []byte {
	if m != nil {
		return m.Title
	}
	return nil
}

func (m *WikiUpdatePageRequest) GetFormat() string {
	if m != nil {
		return m.Format
	}
	return ""
}

func (m *WikiUpdatePageRequest) GetCommitDetails() *WikiCommitDetails {
	if m != nil {
		return m.CommitDetails
	}
	return nil
}

func (m *WikiUpdatePageRequest) GetContent() []byte {
	if m != nil {
		return m.Content
	}
	return nil
}

type WikiUpdatePageResponse struct {
	Error []byte `protobuf:"bytes,1,opt,name=error,proto3" json:"error,omitempty"`
}

func (m *WikiUpdatePageResponse) Reset()                    { *m = WikiUpdatePageResponse{} }
func (m *WikiUpdatePageResponse) String() string            { return proto.CompactTextString(m) }
func (*WikiUpdatePageResponse) ProtoMessage()               {}
func (*WikiUpdatePageResponse) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{8} }

func (m *WikiUpdatePageResponse) GetError() []byte {
	if m != nil {
		return m.Error
	}
	return nil
}

type WikiDeletePageRequest struct {
	Repository    *Repository        `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
	PagePath      []byte             `protobuf:"bytes,2,opt,name=page_path,json=pagePath,proto3" json:"page_path,omitempty"`
	CommitDetails *WikiCommitDetails `protobuf:"bytes,3,opt,name=commit_details,json=commitDetails" json:"commit_details,omitempty"`
}

func (m *WikiDeletePageRequest) Reset()                    { *m = WikiDeletePageRequest{} }
func (m *WikiDeletePageRequest) String() string            { return proto.CompactTextString(m) }
func (*WikiDeletePageRequest) ProtoMessage()               {}
func (*WikiDeletePageRequest) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{9} }

func (m *WikiDeletePageRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *WikiDeletePageRequest) GetPagePath() []byte {
	if m != nil {
		return m.PagePath
	}
	return nil
}

func (m *WikiDeletePageRequest) GetCommitDetails() *WikiCommitDetails {
	if m != nil {
		return m.CommitDetails
	}
	return nil
}

type WikiDeletePageResponse struct {
}

func (m *WikiDeletePageResponse) Reset()                    { *m = WikiDeletePageResponse{} }
func (m *WikiDeletePageResponse) String() string            { return proto.CompactTextString(m) }
func (*WikiDeletePageResponse) ProtoMessage()               {}
func (*WikiDeletePageResponse) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{10} }

type WikiFindPageRequest struct {
	Repository *Repository `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
	Title      []byte      `protobuf:"bytes,2,opt,name=title,proto3" json:"title,omitempty"`
	Revision   []byte      `protobuf:"bytes,3,opt,name=revision,proto3" json:"revision,omitempty"`
	Directory  []byte      `protobuf:"bytes,4,opt,name=directory,proto3" json:"directory,omitempty"`
}

func (m *WikiFindPageRequest) Reset()                    { *m = WikiFindPageRequest{} }
func (m *WikiFindPageRequest) String() string            { return proto.CompactTextString(m) }
func (*WikiFindPageRequest) ProtoMessage()               {}
func (*WikiFindPageRequest) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{11} }

func (m *WikiFindPageRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *WikiFindPageRequest) GetTitle() []byte {
	if m != nil {
		return m.Title
	}
	return nil
}

func (m *WikiFindPageRequest) GetRevision() []byte {
	if m != nil {
		return m.Revision
	}
	return nil
}

func (m *WikiFindPageRequest) GetDirectory() []byte {
	if m != nil {
		return m.Directory
	}
	return nil
}

// WikiFindPageResponse is a stream because we need multiple WikiPage
// messages to send the raw_data field.
type WikiFindPageResponse struct {
	Page *WikiPage `protobuf:"bytes,1,opt,name=page" json:"page,omitempty"`
}

func (m *WikiFindPageResponse) Reset()                    { *m = WikiFindPageResponse{} }
func (m *WikiFindPageResponse) String() string            { return proto.CompactTextString(m) }
func (*WikiFindPageResponse) ProtoMessage()               {}
func (*WikiFindPageResponse) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{12} }

func (m *WikiFindPageResponse) GetPage() *WikiPage {
	if m != nil {
		return m.Page
	}
	return nil
}

type WikiFindFileRequest struct {
	Repository *Repository `protobuf:"bytes,1,opt,name=repository" json:"repository,omitempty"`
	Name       []byte      `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	// Optional: revision
	Revision []byte `protobuf:"bytes,3,opt,name=revision,proto3" json:"revision,omitempty"`
}

func (m *WikiFindFileRequest) Reset()                    { *m = WikiFindFileRequest{} }
func (m *WikiFindFileRequest) String() string            { return proto.CompactTextString(m) }
func (*WikiFindFileRequest) ProtoMessage()               {}
func (*WikiFindFileRequest) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{13} }

func (m *WikiFindFileRequest) GetRepository() *Repository {
	if m != nil {
		return m.Repository
	}
	return nil
}

func (m *WikiFindFileRequest) GetName() []byte {
	if m != nil {
		return m.Name
	}
	return nil
}

func (m *WikiFindFileRequest) GetRevision() []byte {
	if m != nil {
		return m.Revision
	}
	return nil
}

type WikiFindFileResponse struct {
	// If 'name' is empty, the file was not found.
	Name     []byte `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	MimeType string `protobuf:"bytes,2,opt,name=mime_type,json=mimeType" json:"mime_type,omitempty"`
	RawData  []byte `protobuf:"bytes,3,opt,name=raw_data,json=rawData,proto3" json:"raw_data,omitempty"`
	Path     []byte `protobuf:"bytes,4,opt,name=path,proto3" json:"path,omitempty"`
}

func (m *WikiFindFileResponse) Reset()                    { *m = WikiFindFileResponse{} }
func (m *WikiFindFileResponse) String() string            { return proto.CompactTextString(m) }
func (*WikiFindFileResponse) ProtoMessage()               {}
func (*WikiFindFileResponse) Descriptor() ([]byte, []int) { return fileDescriptor12, []int{14} }

func (m *WikiFindFileResponse) GetName() []byte {
	if m != nil {
		return m.Name
	}
	return nil
}

func (m *WikiFindFileResponse) GetMimeType() string {
	if m != nil {
		return m.MimeType
	}
	return ""
}

func (m *WikiFindFileResponse) GetRawData() []byte {
	if m != nil {
		return m.RawData
	}
	return nil
}

func (m *WikiFindFileResponse) GetPath() []byte {
	if m != nil {
		return m.Path
	}
	return nil
}

func init() {
	proto.RegisterType((*WikiCommitDetails)(nil), "gitaly.WikiCommitDetails")
	proto.RegisterType((*WikiPageVersion)(nil), "gitaly.WikiPageVersion")
	proto.RegisterType((*WikiPage)(nil), "gitaly.WikiPage")
	proto.RegisterType((*WikiGetPageVersionsRequest)(nil), "gitaly.WikiGetPageVersionsRequest")
	proto.RegisterType((*WikiGetPageVersionsResponse)(nil), "gitaly.WikiGetPageVersionsResponse")
	proto.RegisterType((*WikiWritePageRequest)(nil), "gitaly.WikiWritePageRequest")
	proto.RegisterType((*WikiWritePageResponse)(nil), "gitaly.WikiWritePageResponse")
	proto.RegisterType((*WikiUpdatePageRequest)(nil), "gitaly.WikiUpdatePageRequest")
	proto.RegisterType((*WikiUpdatePageResponse)(nil), "gitaly.WikiUpdatePageResponse")
	proto.RegisterType((*WikiDeletePageRequest)(nil), "gitaly.WikiDeletePageRequest")
	proto.RegisterType((*WikiDeletePageResponse)(nil), "gitaly.WikiDeletePageResponse")
	proto.RegisterType((*WikiFindPageRequest)(nil), "gitaly.WikiFindPageRequest")
	proto.RegisterType((*WikiFindPageResponse)(nil), "gitaly.WikiFindPageResponse")
	proto.RegisterType((*WikiFindFileRequest)(nil), "gitaly.WikiFindFileRequest")
	proto.RegisterType((*WikiFindFileResponse)(nil), "gitaly.WikiFindFileResponse")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for WikiService service

type WikiServiceClient interface {
	WikiGetPageVersions(ctx context.Context, in *WikiGetPageVersionsRequest, opts ...grpc.CallOption) (WikiService_WikiGetPageVersionsClient, error)
	WikiWritePage(ctx context.Context, opts ...grpc.CallOption) (WikiService_WikiWritePageClient, error)
	WikiUpdatePage(ctx context.Context, opts ...grpc.CallOption) (WikiService_WikiUpdatePageClient, error)
	WikiDeletePage(ctx context.Context, in *WikiDeletePageRequest, opts ...grpc.CallOption) (*WikiDeletePageResponse, error)
	// WikiFindPage returns a stream because the page's raw_data field may be arbitrarily large.
	WikiFindPage(ctx context.Context, in *WikiFindPageRequest, opts ...grpc.CallOption) (WikiService_WikiFindPageClient, error)
	WikiFindFile(ctx context.Context, in *WikiFindFileRequest, opts ...grpc.CallOption) (WikiService_WikiFindFileClient, error)
}

type wikiServiceClient struct {
	cc *grpc.ClientConn
}

func NewWikiServiceClient(cc *grpc.ClientConn) WikiServiceClient {
	return &wikiServiceClient{cc}
}

func (c *wikiServiceClient) WikiGetPageVersions(ctx context.Context, in *WikiGetPageVersionsRequest, opts ...grpc.CallOption) (WikiService_WikiGetPageVersionsClient, error) {
	stream, err := grpc.NewClientStream(ctx, &_WikiService_serviceDesc.Streams[0], c.cc, "/gitaly.WikiService/WikiGetPageVersions", opts...)
	if err != nil {
		return nil, err
	}
	x := &wikiServiceWikiGetPageVersionsClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type WikiService_WikiGetPageVersionsClient interface {
	Recv() (*WikiGetPageVersionsResponse, error)
	grpc.ClientStream
}

type wikiServiceWikiGetPageVersionsClient struct {
	grpc.ClientStream
}

func (x *wikiServiceWikiGetPageVersionsClient) Recv() (*WikiGetPageVersionsResponse, error) {
	m := new(WikiGetPageVersionsResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *wikiServiceClient) WikiWritePage(ctx context.Context, opts ...grpc.CallOption) (WikiService_WikiWritePageClient, error) {
	stream, err := grpc.NewClientStream(ctx, &_WikiService_serviceDesc.Streams[1], c.cc, "/gitaly.WikiService/WikiWritePage", opts...)
	if err != nil {
		return nil, err
	}
	x := &wikiServiceWikiWritePageClient{stream}
	return x, nil
}

type WikiService_WikiWritePageClient interface {
	Send(*WikiWritePageRequest) error
	CloseAndRecv() (*WikiWritePageResponse, error)
	grpc.ClientStream
}

type wikiServiceWikiWritePageClient struct {
	grpc.ClientStream
}

func (x *wikiServiceWikiWritePageClient) Send(m *WikiWritePageRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *wikiServiceWikiWritePageClient) CloseAndRecv() (*WikiWritePageResponse, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(WikiWritePageResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *wikiServiceClient) WikiUpdatePage(ctx context.Context, opts ...grpc.CallOption) (WikiService_WikiUpdatePageClient, error) {
	stream, err := grpc.NewClientStream(ctx, &_WikiService_serviceDesc.Streams[2], c.cc, "/gitaly.WikiService/WikiUpdatePage", opts...)
	if err != nil {
		return nil, err
	}
	x := &wikiServiceWikiUpdatePageClient{stream}
	return x, nil
}

type WikiService_WikiUpdatePageClient interface {
	Send(*WikiUpdatePageRequest) error
	CloseAndRecv() (*WikiUpdatePageResponse, error)
	grpc.ClientStream
}

type wikiServiceWikiUpdatePageClient struct {
	grpc.ClientStream
}

func (x *wikiServiceWikiUpdatePageClient) Send(m *WikiUpdatePageRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *wikiServiceWikiUpdatePageClient) CloseAndRecv() (*WikiUpdatePageResponse, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(WikiUpdatePageResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *wikiServiceClient) WikiDeletePage(ctx context.Context, in *WikiDeletePageRequest, opts ...grpc.CallOption) (*WikiDeletePageResponse, error) {
	out := new(WikiDeletePageResponse)
	err := grpc.Invoke(ctx, "/gitaly.WikiService/WikiDeletePage", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *wikiServiceClient) WikiFindPage(ctx context.Context, in *WikiFindPageRequest, opts ...grpc.CallOption) (WikiService_WikiFindPageClient, error) {
	stream, err := grpc.NewClientStream(ctx, &_WikiService_serviceDesc.Streams[3], c.cc, "/gitaly.WikiService/WikiFindPage", opts...)
	if err != nil {
		return nil, err
	}
	x := &wikiServiceWikiFindPageClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type WikiService_WikiFindPageClient interface {
	Recv() (*WikiFindPageResponse, error)
	grpc.ClientStream
}

type wikiServiceWikiFindPageClient struct {
	grpc.ClientStream
}

func (x *wikiServiceWikiFindPageClient) Recv() (*WikiFindPageResponse, error) {
	m := new(WikiFindPageResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *wikiServiceClient) WikiFindFile(ctx context.Context, in *WikiFindFileRequest, opts ...grpc.CallOption) (WikiService_WikiFindFileClient, error) {
	stream, err := grpc.NewClientStream(ctx, &_WikiService_serviceDesc.Streams[4], c.cc, "/gitaly.WikiService/WikiFindFile", opts...)
	if err != nil {
		return nil, err
	}
	x := &wikiServiceWikiFindFileClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type WikiService_WikiFindFileClient interface {
	Recv() (*WikiFindFileResponse, error)
	grpc.ClientStream
}

type wikiServiceWikiFindFileClient struct {
	grpc.ClientStream
}

func (x *wikiServiceWikiFindFileClient) Recv() (*WikiFindFileResponse, error) {
	m := new(WikiFindFileResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

// Server API for WikiService service

type WikiServiceServer interface {
	WikiGetPageVersions(*WikiGetPageVersionsRequest, WikiService_WikiGetPageVersionsServer) error
	WikiWritePage(WikiService_WikiWritePageServer) error
	WikiUpdatePage(WikiService_WikiUpdatePageServer) error
	WikiDeletePage(context.Context, *WikiDeletePageRequest) (*WikiDeletePageResponse, error)
	// WikiFindPage returns a stream because the page's raw_data field may be arbitrarily large.
	WikiFindPage(*WikiFindPageRequest, WikiService_WikiFindPageServer) error
	WikiFindFile(*WikiFindFileRequest, WikiService_WikiFindFileServer) error
}

func RegisterWikiServiceServer(s *grpc.Server, srv WikiServiceServer) {
	s.RegisterService(&_WikiService_serviceDesc, srv)
}

func _WikiService_WikiGetPageVersions_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(WikiGetPageVersionsRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(WikiServiceServer).WikiGetPageVersions(m, &wikiServiceWikiGetPageVersionsServer{stream})
}

type WikiService_WikiGetPageVersionsServer interface {
	Send(*WikiGetPageVersionsResponse) error
	grpc.ServerStream
}

type wikiServiceWikiGetPageVersionsServer struct {
	grpc.ServerStream
}

func (x *wikiServiceWikiGetPageVersionsServer) Send(m *WikiGetPageVersionsResponse) error {
	return x.ServerStream.SendMsg(m)
}

func _WikiService_WikiWritePage_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(WikiServiceServer).WikiWritePage(&wikiServiceWikiWritePageServer{stream})
}

type WikiService_WikiWritePageServer interface {
	SendAndClose(*WikiWritePageResponse) error
	Recv() (*WikiWritePageRequest, error)
	grpc.ServerStream
}

type wikiServiceWikiWritePageServer struct {
	grpc.ServerStream
}

func (x *wikiServiceWikiWritePageServer) SendAndClose(m *WikiWritePageResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *wikiServiceWikiWritePageServer) Recv() (*WikiWritePageRequest, error) {
	m := new(WikiWritePageRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _WikiService_WikiUpdatePage_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(WikiServiceServer).WikiUpdatePage(&wikiServiceWikiUpdatePageServer{stream})
}

type WikiService_WikiUpdatePageServer interface {
	SendAndClose(*WikiUpdatePageResponse) error
	Recv() (*WikiUpdatePageRequest, error)
	grpc.ServerStream
}

type wikiServiceWikiUpdatePageServer struct {
	grpc.ServerStream
}

func (x *wikiServiceWikiUpdatePageServer) SendAndClose(m *WikiUpdatePageResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *wikiServiceWikiUpdatePageServer) Recv() (*WikiUpdatePageRequest, error) {
	m := new(WikiUpdatePageRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _WikiService_WikiDeletePage_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(WikiDeletePageRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(WikiServiceServer).WikiDeletePage(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/gitaly.WikiService/WikiDeletePage",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(WikiServiceServer).WikiDeletePage(ctx, req.(*WikiDeletePageRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _WikiService_WikiFindPage_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(WikiFindPageRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(WikiServiceServer).WikiFindPage(m, &wikiServiceWikiFindPageServer{stream})
}

type WikiService_WikiFindPageServer interface {
	Send(*WikiFindPageResponse) error
	grpc.ServerStream
}

type wikiServiceWikiFindPageServer struct {
	grpc.ServerStream
}

func (x *wikiServiceWikiFindPageServer) Send(m *WikiFindPageResponse) error {
	return x.ServerStream.SendMsg(m)
}

func _WikiService_WikiFindFile_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(WikiFindFileRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(WikiServiceServer).WikiFindFile(m, &wikiServiceWikiFindFileServer{stream})
}

type WikiService_WikiFindFileServer interface {
	Send(*WikiFindFileResponse) error
	grpc.ServerStream
}

type wikiServiceWikiFindFileServer struct {
	grpc.ServerStream
}

func (x *wikiServiceWikiFindFileServer) Send(m *WikiFindFileResponse) error {
	return x.ServerStream.SendMsg(m)
}

var _WikiService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "gitaly.WikiService",
	HandlerType: (*WikiServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "WikiDeletePage",
			Handler:    _WikiService_WikiDeletePage_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "WikiGetPageVersions",
			Handler:       _WikiService_WikiGetPageVersions_Handler,
			ServerStreams: true,
		},
		{
			StreamName:    "WikiWritePage",
			Handler:       _WikiService_WikiWritePage_Handler,
			ClientStreams: true,
		},
		{
			StreamName:    "WikiUpdatePage",
			Handler:       _WikiService_WikiUpdatePage_Handler,
			ClientStreams: true,
		},
		{
			StreamName:    "WikiFindPage",
			Handler:       _WikiService_WikiFindPage_Handler,
			ServerStreams: true,
		},
		{
			StreamName:    "WikiFindFile",
			Handler:       _WikiService_WikiFindFile_Handler,
			ServerStreams: true,
		},
	},
	Metadata: "wiki.proto",
}

func init() { proto.RegisterFile("wiki.proto", fileDescriptor12) }

var fileDescriptor12 = []byte{
	// 762 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xb4, 0x56, 0xcb, 0x6e, 0xd3, 0x4c,
	0x14, 0xae, 0x9b, 0x34, 0x71, 0x4e, 0x6f, 0x7f, 0xe7, 0xef, 0xdf, 0xdf, 0x4d, 0x4a, 0x55, 0x0d,
	0x48, 0x94, 0x4d, 0x04, 0xe9, 0x96, 0x45, 0x25, 0x4a, 0xbb, 0x42, 0x2a, 0x6e, 0xa1, 0xcb, 0x68,
	0x1a, 0x0f, 0xc9, 0xa8, 0x76, 0x6c, 0xc6, 0x93, 0x54, 0x79, 0x04, 0x1e, 0x81, 0x57, 0xe0, 0x71,
	0x78, 0x0b, 0xb6, 0x3c, 0x01, 0x9a, 0x8b, 0xe3, 0xb1, 0x73, 0x41, 0x50, 0xd8, 0x75, 0xce, 0x39,
	0xfe, 0xce, 0xf9, 0xbe, 0x73, 0x69, 0x00, 0xee, 0xd9, 0x1d, 0x6b, 0x27, 0x3c, 0x16, 0x31, 0xaa,
	0xf5, 0x99, 0x20, 0xe1, 0xa4, 0xb9, 0x91, 0x0e, 0x08, 0xa7, 0x81, 0xb6, 0xe2, 0x1b, 0xd8, 0xb9,
	0x61, 0x77, 0xec, 0x55, 0x1c, 0x45, 0x4c, 0x9c, 0x51, 0x41, 0x58, 0x98, 0x22, 0x04, 0xd5, 0x21,
	0x89, 0xa8, 0xe7, 0x1c, 0x39, 0xc7, 0x1b, 0xbe, 0xfa, 0x1b, 0xed, 0xc2, 0x1a, 0x8d, 0x08, 0x0b,
	0xbd, 0x55, 0x65, 0xd4, 0x0f, 0xe4, 0x41, 0x3d, 0xa2, 0x69, 0x4a, 0xfa, 0xd4, 0xab, 0x28, 0x7b,
	0xf6, 0xc4, 0xd7, 0xb0, 0x2d, 0x81, 0x2f, 0x49, 0x9f, 0xbe, 0xa7, 0x3c, 0x65, 0xf1, 0x10, 0x3d,
	0x83, 0x5a, 0x4f, 0xe5, 0x51, 0xc0, 0xeb, 0x9d, 0x9d, 0xb6, 0x2e, 0xa9, 0x7d, 0xc1, 0x84, 0x2e,
	0xc0, 0x37, 0x01, 0x68, 0x0f, 0x6a, 0x1f, 0x62, 0x1e, 0x11, 0xa1, 0xd2, 0x35, 0x7c, 0xf3, 0xc2,
	0xdf, 0x1c, 0x70, 0x33, 0x58, 0xf4, 0x02, 0xea, 0x63, 0x0d, 0x6d, 0x00, 0xff, 0xcf, 0x00, 0x4b,
	0x99, 0xfd, 0x2c, 0x6e, 0x11, 0xae, 0x64, 0x27, 0x98, 0x08, 0x33, 0x16, 0xfa, 0x81, 0xf6, 0xc1,
	0x1d, 0xf1, 0xb0, 0x9b, 0x10, 0x31, 0xf0, 0xaa, 0x2a, 0xbe, 0x3e, 0xe2, 0xe1, 0x25, 0x11, 0x03,
	0x29, 0x91, 0x32, 0xaf, 0x69, 0x89, 0x12, 0x63, 0x53, 0xb2, 0xd5, 0x2c, 0xd9, 0x0e, 0x01, 0x06,
	0x2c, 0x15, 0x31, 0x67, 0x3d, 0x12, 0x7a, 0xf5, 0x23, 0xe7, 0xd8, 0xf5, 0x2d, 0x8b, 0x4c, 0xc1,
	0xc9, 0x7d, 0x37, 0x20, 0x82, 0x78, 0xae, 0x56, 0x90, 0x93, 0xfb, 0x33, 0x22, 0x08, 0x8e, 0xa0,
	0x29, 0x79, 0x5c, 0x50, 0x61, 0x51, 0x49, 0x7d, 0xfa, 0x71, 0x44, 0x53, 0x81, 0x3a, 0x00, 0x9c,
	0x26, 0x71, 0xca, 0x44, 0xcc, 0x27, 0x86, 0x3f, 0xca, 0xf8, 0xfb, 0x53, 0x8f, 0x6f, 0x45, 0xa1,
	0x16, 0x34, 0x12, 0xd2, 0xa7, 0x9a, 0x90, 0xee, 0xa3, 0x2b, 0x0d, 0x92, 0x11, 0xf6, 0xa1, 0x35,
	0x37, 0x5d, 0x9a, 0xc4, 0xc3, 0x94, 0xa2, 0x13, 0x70, 0x8d, 0x88, 0xa9, 0xe7, 0x1c, 0x55, 0x96,
	0xa9, 0x3d, 0x0d, 0xc4, 0x5f, 0x1d, 0xd8, 0x95, 0xde, 0x1b, 0xce, 0x04, 0x95, 0x21, 0x0f, 0xa9,
	0x3e, 0x93, 0x77, 0xd5, 0x92, 0x37, 0xef, 0x67, 0xa5, 0xd0, 0xcf, 0x53, 0xd8, 0xd2, 0x93, 0xd4,
	0x0d, 0xf4, 0x4c, 0xab, 0xfe, 0xad, 0x77, 0xf6, 0xed, 0x9a, 0x0b, 0x43, 0xef, 0x6f, 0xf6, 0x0a,
	0x3b, 0xe0, 0x41, 0xbd, 0x17, 0x0f, 0x05, 0x1d, 0x0a, 0xd3, 0xe3, 0xec, 0x89, 0x4f, 0xe1, 0xbf,
	0x12, 0x27, 0x23, 0xd1, 0x53, 0xd8, 0x0e, 0x46, 0x49, 0xc8, 0x7a, 0x44, 0xd0, 0x2e, 0xe5, 0x3c,
	0xe6, 0x66, 0x83, 0xb6, 0xa6, 0xe6, 0xd7, 0xd2, 0x8a, 0xbf, 0x3b, 0x1a, 0xe2, 0x5d, 0x12, 0x90,
	0x87, 0xeb, 0xb2, 0xac, 0xab, 0x0b, 0x06, 0x3b, 0x97, 0xad, 0xfa, 0x13, 0xd9, 0xd6, 0x7e, 0x5f,
	0xb6, 0x5a, 0x51, 0xb6, 0x36, 0xec, 0x95, 0x39, 0x1b, 0xdd, 0xe4, 0x69, 0xb1, 0xd4, 0xd2, 0x0f,
	0xfc, 0xc5, 0x88, 0x74, 0x46, 0x43, 0xfa, 0x97, 0x45, 0x9a, 0xa5, 0x5d, 0xf9, 0x35, 0xda, 0xd8,
	0xd3, 0xe4, 0xec, 0x5a, 0x35, 0x39, 0xfc, 0xd9, 0x81, 0x7f, 0xa5, 0xeb, 0x9c, 0x0d, 0x83, 0x87,
	0x92, 0x98, 0x36, 0x73, 0xd5, 0x6e, 0x66, 0x13, 0x5c, 0x4e, 0xc7, 0x4c, 0xdd, 0x41, 0xdd, 0xe5,
	0xe9, 0x1b, 0x1d, 0x40, 0x23, 0x60, 0x9c, 0xf6, 0x54, 0x92, 0xaa, 0x72, 0xe6, 0x06, 0xfc, 0x52,
	0x6f, 0x67, 0x5e, 0x9a, 0x69, 0xc8, 0x13, 0x79, 0xdc, 0xfa, 0xd4, 0x54, 0xf5, 0x4f, 0x79, 0xcf,
	0x7d, 0xe5, 0xc5, 0x93, 0x9c, 0xd8, 0x39, 0x0b, 0xff, 0xf8, 0x6a, 0x2f, 0xa1, 0x85, 0xc7, 0x79,
	0xe1, 0x3a, 0xb5, 0x29, 0x7c, 0xde, 0x3f, 0xae, 0x16, 0x34, 0x22, 0x16, 0xd1, 0xae, 0x98, 0x24,
	0xd4, 0x5c, 0x7d, 0x57, 0x1a, 0xae, 0x27, 0x09, 0x2d, 0x9c, 0xdf, 0x4a, 0xe1, 0xfc, 0x4e, 0x2f,
	0x7c, 0x35, 0xbf, 0xf0, 0x9d, 0x4f, 0x55, 0x58, 0x97, 0x89, 0xaf, 0x28, 0x1f, 0xb3, 0x1e, 0x45,
	0xb7, 0x5a, 0x82, 0xd2, 0xcd, 0x44, 0xd8, 0x56, 0x6c, 0xfe, 0xfd, 0x6e, 0x3e, 0x5e, 0x1a, 0x63,
	0x86, 0x67, 0xe5, 0xb9, 0x83, 0x2e, 0x61, 0xb3, 0x70, 0x6e, 0xd0, 0x81, 0xfd, 0x65, 0xf9, 0xb2,
	0x36, 0x1f, 0x2d, 0xf0, 0x66, 0x88, 0xc7, 0x0e, 0xba, 0x82, 0xad, 0xe2, 0x26, 0xa2, 0xc2, 0x47,
	0x33, 0x57, 0xa9, 0x79, 0xb8, 0xc8, 0x6d, 0x81, 0xbe, 0xd5, 0xa0, 0xf9, 0x06, 0x14, 0x41, 0x67,
	0xb6, 0xb8, 0x08, 0x3a, 0x67, 0x71, 0x56, 0xd0, 0x1b, 0xd8, 0xb0, 0xc7, 0x13, 0xb5, 0xec, 0x2f,
	0x4a, 0xfb, 0xd4, 0x3c, 0x98, 0xef, 0xb4, 0x84, 0xb4, 0xe0, 0xe4, 0xd0, 0xcc, 0xc2, 0x59, 0x53,
	0x3c, 0x0b, 0x67, 0xcf, 0x99, 0x84, 0xbb, 0xad, 0xa9, 0x1f, 0x50, 0x27, 0x3f, 0x02, 0x00, 0x00,
	0xff, 0xff, 0x31, 0xe9, 0x5a, 0x6d, 0x64, 0x09, 0x00, 0x00,
}
